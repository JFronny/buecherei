FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build-env
WORKDIR /app

COPY . ./

RUN dotnet publish Library/Library.csproj -c Release -o out
RUN rm out/Library
RUN chmod a+x /app/dockerstart.sh

FROM mcr.microsoft.com/dotnet/aspnet:8.0-bookworm-slim-arm64v8
WORKDIR /app
COPY --from=build-env /app/out .
COPY --from=build-env /app/dockerstart.sh .
ENTRYPOINT ["/app/dockerstart.sh"]