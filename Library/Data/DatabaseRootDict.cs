using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;

namespace Library.Data
{
    public class DatabaseRootDict : SaveLoadDict<DateTime, DatabaseDayDict>
    {
        private readonly string _dir;
        public DatabaseRootDict(string dir) => _dir = dir;
        protected override void Save(IDictionary<DateTime, DatabaseDayDict> v) => PrepD();
        protected override IDictionary<DateTime, DatabaseDayDict> Load()
        {
            PrepD();
            Dictionary<DateTime, DatabaseDayDict> dictionary = new();
            foreach (string s in Directory.GetDirectories(_dir))
            {
                DateTime date = DateTime.Parse(Path.GetFileName(s)).Date;
                dictionary.Add(date, new DatabaseDayDict(s, date));
            }
            return dictionary;
        }

        private void PrepD()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            if (!Directory.Exists(_dir))
                Directory.CreateDirectory(_dir);
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("de-DE");
        }

        public DatabaseDayDict Get(DateTime date)
        {
            PrepD();
            date = date.Date;
            lock (this)
                if (!ContainsKey(date))
                {
                    string dir = Path.Combine(_dir, date.ToString("yyyy-MM-dd"));
                    Add(date, new DatabaseDayDict(dir, date));
                    Directory.CreateDirectory(dir);
                }
            return this[date];
        }

        public void Clean()
        {
            foreach (string s in Directory.GetDirectories(_dir))
                if (Directory.GetFiles(s).Length == 0)
                    Directory.Delete(s);
        }
    }
}