using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Library.Data
{
    public class SessionState
    {
        public string AuthHash { get; set; } = "";

        public static string Hash(string text)
        {
            text ??= "";
            using MD5 sec = new MD5CryptoServiceProvider();
            Encoding utf8 = Encoding.UTF8;
            return BitConverter.ToString(sec.ComputeHash(utf8.GetBytes(text)));
        }

        public bool Authenticated => AuthHash == Environment.GetEnvironmentVariable(Program.PasswordHash);

        public KeyValuePair<DateTime, KeyValuePair<DateTime, Entry>> CurrentEntry = new(DateTime.Today, new KeyValuePair<DateTime, Entry>(DateTime.Now, new Entry()));
    }
}