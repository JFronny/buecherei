namespace Library.Data
{
    public class Entry
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
    }
}