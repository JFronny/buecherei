using System;
using System.Collections;
using System.Collections.Generic;

namespace Library.Data
{
    public abstract class SaveLoadDict<T, U> : IDictionary<T, U>
    {
        /// <inheritdoc />
        public IEnumerator<KeyValuePair<T, U>> GetEnumerator()
        {
            lock (this) return Load().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            lock (this) return GetEnumerator();
        }

        /// <inheritdoc />
        public void Add(KeyValuePair<T, U> item)
        {
            lock (this) Add(item.Key, item.Value);
        }

        /// <inheritdoc />
        public void Clear()
        {
            lock (this) Save(new Dictionary<T, U>());
        }

        /// <inheritdoc />
        public bool Contains(KeyValuePair<T, U> item)
        {
            lock (this) return Load().Contains(item);
        }

        /// <inheritdoc />
        public void CopyTo(KeyValuePair<T, U>[] array, int arrayIndex)
        {
            lock (this) Load().CopyTo(array, arrayIndex);
        }

        /// <inheritdoc />
        public bool Remove(KeyValuePair<T, U> item)
        {
            lock (this)
            {
                IDictionary<T, U> dictionary = Load();
                try
                {
                    return dictionary.Remove(item);
                }
                finally
                {
                    Save(dictionary);
                }
            }
        }

        /// <inheritdoc />
        public int Count
        {
            get { lock (this) return Load().Count; }
        }

        /// <inheritdoc />
        public bool IsReadOnly => false;

        /// <inheritdoc />
        public void Add(T key, U value)
        {
            lock (this)
            {
                IDictionary<T, U> dictionary = Load();
                dictionary.Add(key, value);
                Save(dictionary);
            }
        }

        /// <inheritdoc />
        public bool ContainsKey(T key)
        {
            lock (this) return Load().ContainsKey(key);
        }

        /// <inheritdoc />
        public bool Remove(T key)
        {
            lock (this)
            {
                IDictionary<T, U> dictionary = Load();
                try
                {
                    return dictionary.Remove(key);
                }
                finally
                {
                    Save(dictionary);
                }
            }
        }

        /// <inheritdoc />
        public bool TryGetValue(T key, out U value)
        {
            lock (this) return Load().TryGetValue(key, out value);
        }

        /// <inheritdoc />
        public U this[T key]
        {
            get
            {
                lock (this) return Load()[key];
            }
            set
            {
                lock (this)
                {
                    IDictionary<T, U> dictionary = Load();
                    dictionary[key] = value;
                    Save(dictionary);
                }
            }
        }

        /// <inheritdoc />
        public ICollection<T> Keys
        {
            get { lock (this) return Load().Keys; }
        }

        /// <inheritdoc />
        public ICollection<U> Values
        {
            get { lock (this) return Load().Values; }
        }

        /// <summary>
        /// Replace the current content based on the current state
        /// </summary>
        /// <param name="f">Function to mutate the content</param>
        public void Mutate(Func<IDictionary<T, U>, IDictionary<T, U>> f)
        {
            lock (this) Save(f(Load()));
        }
        /// <summary>
        /// Save the dictionary content
        /// </summary>
        /// <param name="v">Dictionary content to save</param>
        protected abstract void Save(IDictionary<T, U> v);
        /// <summary>
        /// Load the content to a dictionary
        /// </summary>
        /// <returns>The loaded content</returns>
        protected abstract IDictionary<T, U> Load();
    }
}