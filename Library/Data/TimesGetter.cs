using System;
using System.Collections.Generic;
using System.Linq;

namespace Library.Data
{
    public static class TimesGetter
    {
        private static IEnumerable<DateTime> GetTimes(DateTime date)
        {
            date = date.Date;
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Monday:
                case DayOfWeek.Thursday:
                    return new[]
                    {
                        date + new TimeSpan(15, 30, 0),
                        date + new TimeSpan(15, 50, 0),
                        date + new TimeSpan(16, 10, 0),
                        date + new TimeSpan(16, 30, 0),
                        date + new TimeSpan(16, 50, 0),
                        date + new TimeSpan(17, 10, 0),
                        date + new TimeSpan(17, 30, 0),
                        date + new TimeSpan(17, 50, 0),
                        date + new TimeSpan(18, 10, 0)
                    };
                case DayOfWeek.Saturday:
                    return new[]
                    {
                        date + new TimeSpan(10, 0, 0),
                        date + new TimeSpan(10, 20, 0),
                        date + new TimeSpan(10, 40, 0),
                        date + new TimeSpan(11, 00, 0),
                        date + new TimeSpan(11, 20, 0),
                        date + new TimeSpan(11, 40, 0)
                    };
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static IEnumerable<DateTime> GetTimesAvailable(DateTime date) => GetTimes(date)
            .Where(s => !DbUtil.Entries.Get(date).ContainsKey(s) && s >= DateTime.Now);
    }
}