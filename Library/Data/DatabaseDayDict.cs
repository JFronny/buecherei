using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.Json;
using System.Threading;

namespace Library.Data
{
    public class DatabaseDayDict : SaveLoadDict<DateTime, Entry>
    {
        private readonly DateTime _date;
        public string Dir { get; }
        private string DisableFile => Path.Combine(Dir, ".disabled");
        public bool Enabled
        {
            get => !File.Exists(DisableFile);
            set
            {
                if (value != Enabled)
                {
                    if (value) File.Delete(DisableFile);
                    else File.Create(DisableFile).Dispose();
                }
            }
        }

        public DatabaseDayDict(string dir, DateTime date)
        {
            Dir = dir;
            _date = date.Date;
        }
        protected override void Save(IDictionary<DateTime, Entry> v)
        {
            Directory.Delete(Dir, true);
            PrepD();
            if (v.Count > 0)
                foreach (KeyValuePair<DateTime, Entry> kvp in v)
                    File.WriteAllText(Path.Combine(Dir, kvp.Key.ToShortTimeString()), JsonSerializer.Serialize(kvp.Value));
        }

        protected override IDictionary<DateTime, Entry> Load()
        {
            PrepD();
            Dictionary<DateTime, Entry> dictionary = new();
            foreach (string file in Directory.GetFiles(Dir))
            {
                string name = Path.GetFileName(file);
                if (name != ".disabled")
                {
                    DateTime dt = DateTime.Parse(name);
                    dictionary.Add(_date.AddHours(dt.Hour).AddMinutes(dt.Minute).AddSeconds(dt.Second),
                        JsonSerializer.Deserialize<Entry>(File.ReadAllText(file)));
                }
            }
            return dictionary;
        }

        private void PrepD()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            if (!Directory.Exists(Dir))
                Directory.CreateDirectory(Dir);
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("de-DE");
        }
    }
}