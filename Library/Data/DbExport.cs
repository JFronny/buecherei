using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace Library.Data
{
    public static class DbExport
    {
        public static async Task<Stream> Export(DatabaseDayDict dict)
        {
            using ExcelPackage package = new();
            ExcelWorksheet ws = package.Workbook.Worksheets.Add(Path.GetFileName(dict.Dir));
            int row = 1;
            ws.FormatCell(row, 1, "Datum:", true);
            ws.FormatCell(row, 2, Path.GetFileName(dict.Dir));
            row++;
            ws.FormatCell(row, 1, "Aktiviert:", true);
            ws.FormatCell(row, 2, dict.Enabled ? "Ja" : "Nein");
            row++;
            ws.FormatCell(row, 1, "Teilnehmerzahl:", true);
            ws.FormatCell(row, 2, dict.Count.ToString());
            row += 2;
            ws.FormatCell(row, 1, "No.", true, true, true);
            ws.FormatCell(row, 2, "Zeit", true, true, true);
            ws.FormatCell(row, 3, "Name", true, true, true);
            ws.FormatCell(row, 4, "Telefon", true, true, true);
            for (int i = 0; i < dict.ToArray().Length; i++)
            {
                (DateTime dateTime, Entry entry) = dict.ToArray()[i];
                row++;
                ws.FormatCell(row, 1, (i + 1).ToString(), table:true);
                ws.FormatCell(row, 2, dateTime.ToLongTimeString(), table:true);
                ws.FormatCell(row, 3, entry.Name, table:true);
                ws.FormatCell(row, 4, entry.PhoneNumber, table:true);
            }
            for (int i = 1; i < 5; i++) ws.Column(i).AutoFit();
            MemoryStream ms = new();
            await package.SaveAsAsync(ms);
            return ms;
        }

        private static void FormatCell(this ExcelWorksheet ws, int row, int column, string text = null, bool bold = false, bool table = false, bool header = false)
        {
            if (text != null)
            {
                ws.Cells[row, column].Value = text;
                ws.Cells[row, column].Style.Font.Name = "Helvetica";
            }
            if (bold) ws.Cells[row, column].Style.Font.Bold = true;
            if (table) ws.Cells[row, column].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            if (header) ws.Cells[row, column].Style.Fill.SetBackground(Color.Khaki);
        }
    }
}
