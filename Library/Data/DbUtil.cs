using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Library.Data
{
    public static class DbUtil
    {
        private static readonly string BaseDir =
            Path.GetFullPath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));

        private static readonly string DbDir = Path.Combine(BaseDir, "DB");
        public static readonly DatabaseRootDict Entries = new(DbDir);
        private static readonly TimeSpan MaxFuture = new(30, 0, 0, 0);
        public static DateTime NextVisible(DateTime b)
        {
            if (b <= DateTime.Today)
                b = b.AddDays((DateTime.Today - b).Days);
            if (b > DateTime.Today + MaxFuture)
                while (!DateVisible(b))
                    b = b.AddDays(-1);
            while (!DateVisible(b))
                b = b.AddDays(1);
            return b;
        }
        public static DateTime NextVisibleAdmin(DateTime b)
        {
            if (b > DateTime.Today + MaxFuture)
                while (!DateVisibleAdmin(b))
                    b = b.AddDays(-1);
            while (!DateVisibleAdmin(b))
                b = b.AddDays(1);
            return b;
        }

        public static bool DateVisible(DateTime date) =>
            DateVisibleAdmin(date)
            && TimesGetter.GetTimesAvailable(date).Any()
            && DateTime.Today <= date
            && Entries.Get(date).Enabled;

        public static bool DateVisibleAdmin(DateTime date) => new[]
            {
                DayOfWeek.Monday,
                DayOfWeek.Thursday,
                DayOfWeek.Saturday
            }.Contains(date.DayOfWeek)
            && DateTime.Today + MaxFuture * 2 > date;
    }
}