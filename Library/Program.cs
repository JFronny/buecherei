using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Library.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;

namespace Library
{
    public class Program
    {
        public const string PasswordHash = "PASSWORD_HASH";
        public static void Main(string[] args)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.GetCultureInfo("de-DE");
            if (args.Contains("--hash"))
            {
                int i = args.ToList().IndexOf("--hash") + 1;
                Console.WriteLine(i >= args.Length ? "Usage: --hash [text]" : SessionState.Hash(args[i]));
                return;
            }
            Environment.SetEnvironmentVariable(PasswordHash, Environment.GetEnvironmentVariable(PasswordHash) ?? throw new Exception($"{PasswordHash} environment variable not set"));
            DbUtil.Entries.Clean();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}